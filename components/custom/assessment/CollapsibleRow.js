import { useState } from "react";
import classnames from "classnames";
import { ChevronUp } from "react-feather";
import {
  Collapse,
  Card,
  CardHeader,
  CardBody,
  Table,
  FormGroup,
  Input,
  Badge,
} from "reactstrap";
import Select from "react-select";
import { selectThemeColors } from "utility/Utils";

const CollapsibleRow = ({ data, active, type }) => {
  const defaultActive = () => [active];

  const [openCollapse, setOpenCollapse] = useState(defaultActive());

  const handleCollapseToggle = (id) => {
    const arr = openCollapse,
      index = arr.indexOf(id);
    if (arr.includes(id)) {
      arr.splice(index, 1);
      setOpenCollapse([...arr]);
    } else {
      arr.push(id);
      setOpenCollapse([...arr]);
    }
  };

  const scoreOptions = [
    { value: 0, label: "0" },
    { value: 1, label: "1" },
    { value: 2, label: "2" },
    { value: 3, label: "3" },
    { value: 4, label: "4" },
  ];

  return (
    <Table bordered borderless>
      <tbody
        className={classnames("collapse-icon", {
          "collapse-default": !type,
        })}
      >
        {data &&
          data.map((item, index) => (
            <Card
              className={classnames("app-collapse", {
                open: openCollapse.includes(index),
              })}
              key={index}
            >
              <CardHeader
                className={classnames("align-items-center p-0", {
                  collapsed: !openCollapse.includes(index),
                })}
                onClick={() => handleCollapseToggle(index)}
              >
                <tr className="d-flex align-items-center">
                  <td style={{ width: "5%" }}>
                    <ChevronUp size={14} />
                  </td>
                  <td className="font-weight-bolder" style={{ width: "25%" }}>
                    {item.item}
                  </td>
                  <td className="px-0 align-middle" style={{ width: "45%" }}>
                    <ol>
                      {item.criteria &&
                        item.criteria.map((criteria) => (
                          <li key={criteria.id} className="text-left">
                            {criteria.desc}
                          </li>
                        ))}
                    </ol>
                  </td>
                  <td style={{ width: "10%" }}>
                    <Select
                      theme={selectThemeColors}
                      className="react-select"
                      classNamePrefix="select"
                      defaultValue={scoreOptions[0]}
                      options={scoreOptions}
                      isClearable={false}
                    />
                  </td>
                  <td className="text-center" style={{ width: "7.5%" }}>
                    {item.weight}
                  </td>
                  <td className="text-center" style={{ width: "7.5%" }}>
                    {item.weight * scoreOptions.value}
                  </td>
                </tr>
              </CardHeader>
              <Collapse isOpen={openCollapse.includes(index)}>
                <CardBody className="m-2 p-2 shadow">
                  <FormGroup className="mb-2">
                    <h5 className="font-weight-bolder">Deskripsi</h5>
                    <Input
                      type="textarea"
                      id="deskripsiItem"
                      rows="3"
                      value={item.content.desc}
                    />
                  </FormGroup>
                  <div className="mb-2">
                    <h5 className="font-weight-bolder">
                      Kreativitas dapat dinilai ANTARA LAIN melalui
                    </h5>
                    <ul>
                      <li>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                        sed do eiusmod tempor incididunt ut labore
                      </li>
                      <li>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                        sed do eiusmod tempor incididunt ut labore
                      </li>
                      <li>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                        sed do eiusmod tempor incididunt ut labore
                      </li>
                    </ul>
                  </div>
                  <Table bordered>
                    <thead>
                      <tr>
                        <th>Status</th>
                        <th>Score</th>
                        <th>Description</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>
                          <Badge pill color="light-danger">
                            Low
                          </Badge>
                        </td>
                        <td className="text-danger">10-40</td>
                        <td>
                          Tema hanya menjawab tantangan bisnis jangka pendek
                          (1-2 tahun ke depan)
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <Badge pill color="light-warning">
                            Medium
                          </Badge>
                        </td>
                        <td className="text-warning">50-70</td>
                        <td>
                          Tema hanya menjawab tantangan bisnis jangka menengah
                          dan panjang (3-5 tahun ke depan)
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <Badge pill color="light-success">
                            High
                          </Badge>
                        </td>
                        <td className="text-success">80-100</td>
                        <td>
                          Tema hanya menjawab tantangan bisnis jangka pendek,
                          menengah, dan panjang (1-5 tahun ke depan)
                        </td>
                      </tr>
                    </tbody>
                  </Table>
                </CardBody>
              </Collapse>
            </Card>
          ))}
      </tbody>
    </Table>
  );
};

export default CollapsibleRow;
