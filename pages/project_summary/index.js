import { Fragment } from 'react'
import { Form, Label, Input, FormGroup, Row, Col,Card,
    CardBody,
    CardText,
    Table,
} from 'reactstrap'
import {Trash, Download} from 'react-feather'
const CreateCard = ({text, classname }) => (
    <>
        <Card className='cardcustom'>
        <CardBody className={classname}>
        <CardText className='content-text'>
            {text}
        </CardText>
        </CardBody>
        </Card>
    </>
  );
const ProjectSum = () => {
    return (
        <Fragment>
            <div className='ProjectSum'>
                <div className='PIC'>
                    <div className='content-top'>
                        <div className='mb-2'>I. PIC Information</div>
                    </div>
                    <div className='content-body'>
                        <Form>
                            <Row>
                                <Col>
                                    <Label className='form-label'>Email</Label>
                                    <CreateCard text="arya.mukti@kalbe.co.id" classname="card-sm"/>
                                </Col>
                                <Col>
                                    <Label className='form-label'>Team Name</Label>
                                    <CreateCard text="Consumer Health"  classname="card-sm"/>
                                </Col>
                            </Row>
                        </Form>
                    <hr/>
                    </div>
                </div>
                <div className='Konvensi Inovasi'>
                    <div className='content-top'>
                        <div className='mb-2'>II. Konvensi Inovasi Project</div>
                    </div> 
                    <div className='content-body'>
                        <Form>
                            <Row>
                                <Col>
                                    <Label className='form-label'>Year</Label>
                                    <CreateCard text="2021"  classname="card-sm"/>
                                </Col>
                                <Col>
                                    <Label className='form-label'>Project Theme</Label>
                                    <CreateCard text="Revenue Growth" classname="card-sm"/>
                                </Col>
                            </Row>
                            <Row>
                                <Col>
                                    <Label className='form-label'>Idea References</Label>
                                    <CreateCard text="001/IDEA/X/2021" classname="card-sm"/>
                                </Col>
                                <Col>
                                    <Label className='form-label'>Project Name</Label>
                                    <CreateCard text="KALINA" classname="card-sm"/>
                                </Col>
                            </Row>
                            <Row>
                                <Col>
                                    <Label className='form-label'>Sub Group Category</Label>
                                    <CreateCard text="Exploration - Innovation Concept" classname="card-sm"/>
                                </Col>
                                <Col>
                                    <Label className='form-label'>Team Name</Label>
                                    <CreateCard text="Kners" classname="card-sm"/>
                                </Col>
                            </Row>
                        </Form>
                        <div className='content-label'>Team Member</div>
                        <Table responsive className='tableresponsive'>
                            <thead>
                                <tr>
                                <th className='content-center'>No</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Role</th>
                                <th>SBU</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr className='space-table'>
                                    <td className='content-number' style={{paddingTop:"20px",paddingBottom:"20px"}}>1</td>
                                    <td style={{paddingTop:"20px",paddingBottom:"20px"}}>Andhika</td>
                                    <td style={{paddingTop:"20px",paddingBottom:"20px"}}>Andhika@kalbe.co.id</td>
                                    <td style={{paddingTop:"20px",paddingBottom:"20px"}}>Advisor</td>
                                    <td style={{paddingTop:"20px",paddingBottom:"20px"}}>Corporate</td>
                                </tr>
                                <tr>
                                    <td className='content-number' style={{paddingTop:"20px",paddingBottom:"20px"}}>2</td>
                                    <td style={{paddingTop:"20px",paddingBottom:"20px"}}>Anggoro</td>
                                    <td style={{paddingTop:"20px",paddingBottom:"20px"}}>Anggoro@kalbe.co.id</td>
                                    <td style={{paddingTop:"20px",paddingBottom:"20px"}}>Project Leader</td>
                                    <td style={{paddingTop:"20px",paddingBottom:"20px"}}>Corporate</td>
                                </tr>
                                <tr>
                                    <td className='content-number' style={{paddingTop:"20px",paddingBottom:"20px"}}>3</td>
                                    <td style={{paddingTop:"20px",paddingBottom:"20px"}}>Suhandi</td>
                                    <td style={{paddingTop:"20px",paddingBottom:"20px"}}>Suhandi@kalbe.co.id</td>
                                    <td style={{paddingTop:"20px",paddingBottom:"20px"}}>Team Member</td>
                                    <td style={{paddingTop:"20px",paddingBottom:"20px"}}>Corporate</td>
                                </tr>
                            </tbody>
                        </Table>
                        <div className='content-label'>Background</div>
                            <CreateCard
                                text="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
                                classname="card-lg"
                            />
                        <div className='content-label'>Innovation Goal</div>
                            <CreateCard
                                text="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
                                classname="card-lg"
                            />
                        <div className='content-label'>Project Scope</div>
                            <CreateCard
                                text="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
                                classname="card-lg"
                            />
                        <div className='content-label'>Project Duration</div>
                        <Form>
                            <Row>
                                <Col>
                                    <Label className='form-label'>Start</Label>
                                    <CreateCard text="1 January 2021" classname="card-sm"/>
                                </Col>
                                <Col>
                                    <Label className='form-label'>Finish</Label>
                                    <CreateCard text="31 January 2021" classname="card-sm"/>
                                </Col>
                            </Row> 
                        </Form>
                        <hr/>
                    </div>
                </div>
                <div className='Attachment'>
                        <div className='content-top'>
                            <div className='mb-2'>III. Attachment</div>
                        </div>
                        <div className='content-body'>
                        <Table responsive className='tableresponsive'>
                            <thead>
                                <tr>
                                <th className='content-center'>No</th>
                                <th>File Name</th>
                                <th>Description</th>
                                <th>Upload Date</th>
                                <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td className='content-number' style={{paddingTop:"20px",paddingBottom:"20px"}}>1</td>
                                    <td style={{paddingTop:"20px",paddingBottom:"20px"}}>Kalina-chatbot.pptx</td>
                                    <td style={{paddingTop:"20px",paddingBottom:"20px"}}>Presentasi Final KALINA</td>
                                    <td style={{paddingTop:"20px",paddingBottom:"20px"}}>27 Desember 2020</td>
                                    <td>
                                        <div className="d-flex">
                                            <Trash className='mr-50' size={25} style={{color:'red'}} />
                                            <Download className='mr-50' size={25} style={{color:'green'}}/>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </Table>
                    </div>
                </div>
        </div>
    </Fragment>
    )
}
export default ProjectSum;