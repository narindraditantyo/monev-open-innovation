import { tabsBasic } from "components/vuexy/views/components/tabs/TabSourceCode";
import { Fragment, useEffect, useState } from "react";
import {
  Button,
  Card,
  Collapse,
  Container,
  CustomInput,
  FormGroup,
  Input,
  Label,
  Table,
} from "reactstrap";
import axios from "axios";
import { ChevronUp } from "react-feather";
import Select from "react-select";
import { selectThemeColors } from "utility/Utils";
import CollapsibleRow from "components/custom/assessment/CollapsibleRow";

const Assessment = () => {
  const [data, setData] = useState(null);
  const [isReqGenba, setReqGenba] = useState(false);

  useEffect(() => {
    axios.get("/api/datatables/initial-data").then((response) => {
      console.log(response.data);
      setData(response.data);
    });
  }, []);

  return (
    <Container>
      <div className="mb-3">
        <Table bordered borderless>
          <thead>
            <tr>
              <th style={{ width: "5%" }}></th>
              <th style={{ width: "25%" }}>Item</th>
              <th style={{ width: "45%" }}>Assessment Criteria</th>
              <th className="text-center" style={{ width: "10" }}>Score</th>
              <th className="text-center" style={{ width: "7.5%" }}>Weight</th>
              <th className="text-center" style={{ width: "7.5%" }}>Total</th>
            </tr>
          </thead>
        </Table>
        <CollapsibleRow data={data} />
        <Table bordered borderless>
          <tfoot>
            <tr className="bg-primary bg-lighten-5">
              <td
                className="h3 text-primary font-weight-bolder py-1"
                style={{ width: "92.5%" }}
              >
                Total
              </td>
              <td className="h3 text-primary text-center font-weight-bolder py-1">
                16
              </td>
            </tr>
          </tfoot>
        </Table>
      </div>
      <FormGroup className="mb-3">
        <Label for="catatanJuri">Catatan Juri</Label>
        <Input
          type="textarea"
          id="catatanJuri"
          rows="5"
          placeholder="Enter Email"
        />
      </FormGroup>
      <FormGroup className="mb-3">
        <CustomInput
          type="checkbox"
          className="custom-control-Primary mb-2"
          id="genbaNote"
          label="Request Genba"
          onChange={() => setReqGenba(!isReqGenba)}
        />
        <Collapse isOpen={isReqGenba}>
          <Label for="genbaNote">Genba's Note</Label>
          <Input
            type="textarea"
            id="genbaNote"
            rows="5"
            placeholder="Enter Email"
          />
        </Collapse>
      </FormGroup>
      <div className="d-flex justify-content-end">
        <Button color="primary">Save Changes</Button>
        <div className="ml-1"></div>
        <Button color="primary" outline>
          Cancel
        </Button>
      </div>
    </Container>
  );
};

export default Assessment;
