
import { MoreVertical, Edit, Edit2, Trash, CheckCircle, XCircle, EyeOff, CornerUpLeft, RefreshCw, Clipboard } from 'react-feather'
import { Table, Badge, UncontrolledDropdown, DropdownMenu, DropdownItem, DropdownToggle } from 'reactstrap'

const TableBasic = () => {
  return (
    <Table responsive>
      <thead>
        <tr>
          <th>NO</th>
          <th>YEAR</th>
          <th>CREATOR</th>
          <th>TOPIC</th>
          <th>SUBTOPIC</th>
          <th>TITLE</th>
          <th>TAGGING</th>
          <th>STATUS</th>
          <th>ACTIONS</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>
            <t className='align-middle font-weight-bold' style={{ color: "#ff9f43" }}>1</t>
          </td>
          <td>
            <t className='align-middle font-weight-bold' style={{ color: "#ff9f43" }}>2021</t>
          </td>
          <td>
            <t>Anggoro Wisesso</t>
          </td>
          <td>
            <t>Product</t>
          </td>
          <td>
            <t>Chat Bot</t>
          </td>
          <td>
            <t>Kalina - Customer Service Chat Bot</t>
          </td>
          <td>
            <Badge pill color='light-info' className='mr-1'>
              #chat
            </Badge>
            <Badge pill color='light-info' className='mr-1'>
              #bot
            </Badge>
            <Badge pill color='light-info' className='mr-1'>
              #cs
            </Badge>
          </td>
          <td>
            <Badge pill color='light-success' className='mr-1'>
              Accepted
            </Badge>
          </td>
          <td>
            <UncontrolledDropdown>
              <DropdownToggle className='icon-btn hide-arrow' color='transparent' size='sm' caret>
                <MoreVertical size={15} />
              </DropdownToggle>
              <DropdownMenu right>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <Edit2 className='mr-50' size={15} /> <span className='align-middle'>View Details</span>
                </DropdownItem>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <CheckCircle className='mr-50' size={15} /> <span className='align-middle'>Accept</span>
                </DropdownItem>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <XCircle className='mr-50' size={15} /> <span className='align-middle'>Reject</span>
                </DropdownItem>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <RefreshCw className='mr-50' size={15} /> <span className='align-middle'>Revise</span>
                </DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
          </td>
        </tr>
        <tr>
          <td>
            <t className='align-middle font-weight-bold' style={{ color: "#ff9f43" }}>1</t>
          </td>
          <td>
            <t className='align-middle font-weight-bold' style={{ color: "#ff9f43" }}>2021</t>
          </td>
          <td>
            <t>Anggoro Wisesso</t>
          </td>
          <td>
            <t>Product</t>
          </td>
          <td>
            <t>Chat Bot</t>
          </td>
          <td>
            <t>Kalina - Customer Service Chat Bot</t>
          </td>
          <td>
            <Badge pill color='light-info' className='mr-1'>
              #chat
            </Badge>
            <Badge pill color='light-info' className='mr-1'>
              #bot
            </Badge>
            <Badge pill color='light-info' className='mr-1'>
              #cs
            </Badge>
          </td>
          <td>
            <Badge pill color='light-success' className='mr-1'>
              Accepted
            </Badge>
          </td>
          <td>
            <UncontrolledDropdown>
              <DropdownToggle className='icon-btn hide-arrow' color='transparent' size='sm' caret>
                <MoreVertical size={15} />
              </DropdownToggle>
              <DropdownMenu right>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <Edit2 className='mr-50' size={15} /> <span className='align-middle'>View Details</span>
                </DropdownItem>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <CheckCircle className='mr-50' size={15} /> <span className='align-middle'>Accept</span>
                </DropdownItem>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <XCircle className='mr-50' size={15} /> <span className='align-middle'>Reject</span>
                </DropdownItem>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <RefreshCw className='mr-50' size={15} /> <span className='align-middle'>Revise</span>
                </DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
          </td>
        </tr>
        <tr>
          <td>
            <t className='align-middle font-weight-bold' style={{ color: "#ff9f43" }}>1</t>
          </td>
          <td>
            <t className='align-middle font-weight-bold' style={{ color: "#ff9f43" }}>2021</t>
          </td>
          <td>
            <t>Anggoro Wisesso</t>
          </td>
          <td>
            <t>Product</t>
          </td>
          <td>
            <t>Chat Bot</t>
          </td>
          <td>
            <t>Kalina - Customer Service Chat Bot</t>
          </td>
          <td>
            <Badge pill color='light-info' className='mr-1'>
              #chat
            </Badge>
            <Badge pill color='light-info' className='mr-1'>
              #bot
            </Badge>
            <Badge pill color='light-info' className='mr-1'>
              #cs
            </Badge>
          </td>
          <td>
            <Badge pill color='light-success' className='mr-1'>
              Accepted
            </Badge>
          </td>
          <td>
            <UncontrolledDropdown>
              <DropdownToggle className='icon-btn hide-arrow' color='transparent' size='sm' caret>
                <MoreVertical size={15} />
              </DropdownToggle>
              <DropdownMenu right>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <Edit2 className='mr-50' size={15} /> <span className='align-middle'>View Details</span>
                </DropdownItem>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <CheckCircle className='mr-50' size={15} /> <span className='align-middle'>Accept</span>
                </DropdownItem>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <XCircle className='mr-50' size={15} /> <span className='align-middle'>Reject</span>
                </DropdownItem>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <RefreshCw className='mr-50' size={15} /> <span className='align-middle'>Revise</span>
                </DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
          </td>
        </tr>
        <tr>
          <td>
            <t className='align-middle font-weight-bold' style={{ color: "#ff9f43" }}>1</t>
          </td>
          <td>
            <t className='align-middle font-weight-bold' style={{ color: "#ff9f43" }}>2021</t>
          </td>
          <td>
            <t>Anggoro Wisesso</t>
          </td>
          <td>
            <t>Product</t>
          </td>
          <td>
            <t>Chat Bot</t>
          </td>
          <td>
            <t>Kalina - Customer Service Chat Bot</t>
          </td>
          <td>
            <Badge pill color='light-info' className='mr-1'>
              #chat
            </Badge>
            <Badge pill color='light-info' className='mr-1'>
              #bot
            </Badge>
            <Badge pill color='light-info' className='mr-1'>
              #cs
            </Badge>
          </td>
          <td>
            <Badge pill color='light-danger' className='mr-1'>
              Rejected
            </Badge>
          </td>
          <td>
            <UncontrolledDropdown>
              <DropdownToggle className='icon-btn hide-arrow' color='transparent' size='sm' caret>
                <MoreVertical size={15} />
              </DropdownToggle>
              <DropdownMenu right>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <Edit2 className='mr-50' size={15} /> <span className='align-middle'>View Details</span>
                </DropdownItem>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <CheckCircle className='mr-50' size={15} /> <span className='align-middle'>Accept</span>
                </DropdownItem>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <XCircle className='mr-50' size={15} /> <span className='align-middle'>Reject</span>
                </DropdownItem>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <RefreshCw className='mr-50' size={15} /> <span className='align-middle'>Revise</span>
                </DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
          </td>
        </tr>
        <tr>
          <td>
            <t className='align-middle font-weight-bold' style={{ color: "#ff9f43" }}>1</t>
          </td>
          <td>
            <t className='align-middle font-weight-bold' style={{ color: "#ff9f43" }}>2021</t>
          </td>
          <td>
            <t>Anggoro Wisesso</t>
          </td>
          <td>
            <t>Product</t>
          </td>
          <td>
            <t>Chat Bot</t>
          </td>
          <td>
            <t>Kalina - Customer Service Chat Bot</t>
          </td>
          <td>
            <Badge pill color='light-info' className='mr-1'>
              #chat
            </Badge>
            <Badge pill color='light-info' className='mr-1'>
              #bot
            </Badge>
            <Badge pill color='light-info' className='mr-1'>
              #cs
            </Badge>
          </td>
          <td>
            <Badge pill color='light-danger' className='mr-1'>
              Rejected
            </Badge>
          </td>
          <td>
            <UncontrolledDropdown>
              <DropdownToggle className='icon-btn hide-arrow' color='transparent' size='sm' caret>
                <MoreVertical size={15} />
              </DropdownToggle>
              <DropdownMenu right>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <Edit2 className='mr-50' size={15} /> <span className='align-middle'>View Details</span>
                </DropdownItem>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <CheckCircle className='mr-50' size={15} /> <span className='align-middle'>Accept</span>
                </DropdownItem>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <XCircle className='mr-50' size={15} /> <span className='align-middle'>Reject</span>
                </DropdownItem>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <RefreshCw className='mr-50' size={15} /> <span className='align-middle'>Revise</span>
                </DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
          </td>
        </tr>
        <tr>
          <td>
            <t className='align-middle font-weight-bold' style={{ color: "#ff9f43" }}>1</t>
          </td>
          <td>
            <t className='align-middle font-weight-bold' style={{ color: "#ff9f43" }}>2021</t>
          </td>
          <td>
            <t>Anggoro Wisesso</t>
          </td>
          <td>
            <t>Product</t>
          </td>
          <td>
            <t>Chat Bot</t>
          </td>
          <td>
            <t>Kalina - Customer Service Chat Bot</t>
          </td>
          <td>
            <Badge pill color='light-info' className='mr-1'>
              #chat
            </Badge>
            <Badge pill color='light-info' className='mr-1'>
              #bot
            </Badge>
            <Badge pill color='light-info' className='mr-1'>
              #cs
            </Badge>
          </td>
          <td>
            <Badge pill color='light-danger' className='mr-1'>
              Rejected
            </Badge>
          </td>
          <td>
            <UncontrolledDropdown>
              <DropdownToggle className='icon-btn hide-arrow' color='transparent' size='sm' caret>
                <MoreVertical size={15} />
              </DropdownToggle>
              <DropdownMenu right>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <Edit2 className='mr-50' size={15} /> <span className='align-middle'>View Details</span>
                </DropdownItem>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <CheckCircle className='mr-50' size={15} /> <span className='align-middle'>Accept</span>
                </DropdownItem>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <XCircle className='mr-50' size={15} /> <span className='align-middle'>Reject</span>
                </DropdownItem>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <RefreshCw className='mr-50' size={15} /> <span className='align-middle'>Revise</span>
                </DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
          </td>
        </tr>
        <tr>
          <td>
            <t className='align-middle font-weight-bold' style={{ color: "#ff9f43" }}>1</t>
          </td>
          <td>
            <t className='align-middle font-weight-bold' style={{ color: "#ff9f43" }}>2021</t>
          </td>
          <td>
            <t>Anggoro Wisesso</t>
          </td>
          <td>
            <t>Product</t>
          </td>
          <td>
            <t>Chat Bot</t>
          </td>
          <td>
            <t>Kalina - Customer Service Chat Bot</t>
          </td>
          <td>
            <Badge pill color='light-info' className='mr-1'>
              #chat
            </Badge>
            <Badge pill color='light-info' className='mr-1'>
              #bot
            </Badge>
            <Badge pill color='light-info' className='mr-1'>
              #cs
            </Badge>
          </td>
          <td>
            <Badge pill color='light-warning' className='mr-1'>
              Revise
            </Badge>
          </td>
          <td>
            <UncontrolledDropdown>
              <DropdownToggle className='icon-btn hide-arrow' color='transparent' size='sm' caret>
                <MoreVertical size={15} />
              </DropdownToggle>
              <DropdownMenu right>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <Edit2 className='mr-50' size={15} /> <span className='align-middle'>View Details</span>
                </DropdownItem>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <CheckCircle className='mr-50' size={15} /> <span className='align-middle'>Accept</span>
                </DropdownItem>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <XCircle className='mr-50' size={15} /> <span className='align-middle'>Reject</span>
                </DropdownItem>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <RefreshCw className='mr-50' size={15} /> <span className='align-middle'>Revise</span>
                </DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
          </td>
        </tr>
        <tr>
          <td>
            <t className='align-middle font-weight-bold' style={{ color: "#ff9f43" }}>1</t>
          </td>
          <td>
            <t className='align-middle font-weight-bold' style={{ color: "#ff9f43" }}>2021</t>
          </td>
          <td>
            <t>Anggoro Wisesso</t>
          </td>
          <td>
            <t>Product</t>
          </td>
          <td>
            <t>Chat Bot</t>
          </td>
          <td>
            <t>Kalina - Customer Service Chat Bot</t>
          </td>
          <td>
            <Badge pill color='light-info' className='mr-1'>
              #chat
            </Badge>
            <Badge pill color='light-info' className='mr-1'>
              #bot
            </Badge>
            <Badge pill color='light-info' className='mr-1'>
              #cs
            </Badge>
          </td>
          <td>
            <Badge pill color='light-warning' className='mr-1'>
              Revise
            </Badge>
          </td>
          <td>
            <UncontrolledDropdown>
              <DropdownToggle className='icon-btn hide-arrow' color='transparent' size='sm' caret>
                <MoreVertical size={15} />
              </DropdownToggle>
              <DropdownMenu right>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <Edit2 className='mr-50' size={15} /> <span className='align-middle'>View Details</span>
                </DropdownItem>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <CheckCircle className='mr-50' size={15} /> <span className='align-middle'>Accept</span>
                </DropdownItem>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <XCircle className='mr-50' size={15} /> <span className='align-middle'>Reject</span>
                </DropdownItem>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <RefreshCw className='mr-50' size={15} /> <span className='align-middle'>Revise</span>
                </DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
          </td>
        </tr>
        <tr>
          <td>
            <t className='align-middle font-weight-bold' style={{ color: "#ff9f43" }}>1</t>
          </td>
          <td>
            <t className='align-middle font-weight-bold' style={{ color: "#ff9f43" }}>2021</t>
          </td>
          <td>
            <t>Anggoro Wisesso</t>
          </td>
          <td>
            <t>Product</t>
          </td>
          <td>
            <t>Chat Bot</t>
          </td>
          <td>
            <t>Kalina - Customer Service Chat Bot</t>
          </td>
          <td>
            <Badge pill color='light-info' className='mr-1'>
              #chat
            </Badge>
            <Badge pill color='light-info' className='mr-1'>
              #bot
            </Badge>
            <Badge pill color='light-info' className='mr-1'>
              #cs
            </Badge>
          </td>
          <td>
            <Badge pill color='light-warning' className='mr-1'>
              Revise
            </Badge>
          </td>
          <td>
            <UncontrolledDropdown>
              <DropdownToggle className='icon-btn hide-arrow' color='transparent' size='sm' caret>
                <MoreVertical size={15} />
              </DropdownToggle>
              <DropdownMenu right>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <Edit2 className='mr-50' size={15} /> <span className='align-middle'>View Details</span>
                </DropdownItem>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <CheckCircle className='mr-50' size={15} /> <span className='align-middle'>Accept</span>
                </DropdownItem>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <XCircle className='mr-50' size={15} /> <span className='align-middle'>Reject</span>
                </DropdownItem>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <RefreshCw className='mr-50' size={15} /> <span className='align-middle'>Revise</span>
                </DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
          </td>
        </tr>
        <tr>
          <td>
            <t className='align-middle font-weight-bold' style={{ color: "#ff9f43" }}>1</t>
          </td>
          <td>
            <t className='align-middle font-weight-bold' style={{ color: "#ff9f43" }}>2021</t>
          </td>
          <td>
            <t>Anggoro Wisesso</t>
          </td>
          <td>
            <t>Product</t>
          </td>
          <td>
            <t>Chat Bot</t>
          </td>
          <td>
            <t>Kalina - Customer Service Chat Bot</t>
          </td>
          <td>
            <Badge pill color='light-info' className='mr-1'>
              #chat
            </Badge>
            <Badge pill color='light-info' className='mr-1'>
              #bot
            </Badge>
            <Badge pill color='light-info' className='mr-1'>
              #cs
            </Badge>
          </td>
          <td>
            <Badge pill color='light-warning' className='mr-1'>
              Revise
            </Badge>
          </td>
          <td>
            <UncontrolledDropdown>
              <DropdownToggle className='icon-btn hide-arrow' color='transparent' size='sm' caret>
                <MoreVertical size={15} />
              </DropdownToggle>
              <DropdownMenu right>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <Edit2 className='mr-50' size={15} /> <span className='align-middle'>View Details</span>
                </DropdownItem>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <CheckCircle className='mr-50' size={15} /> <span className='align-middle'>Accept</span>
                </DropdownItem>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <XCircle className='mr-50' size={15} /> <span className='align-middle'>Reject</span>
                </DropdownItem>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <RefreshCw className='mr-50' size={15} /> <span className='align-middle'>Revise</span>
                </DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
          </td>
        </tr>
      </tbody>
    </Table>
  )
}

export default TableBasic

