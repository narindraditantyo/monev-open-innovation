import AvatarGroup from 'components/vuexy/@core/components/avatar-group'
import react from 'public/images/icons/react.svg'
import vuejs from 'public/images/icons/vuejs.svg'
import angular from 'public/images/icons/angular.svg'
import bootstrap from 'public/images/icons/bootstrap.svg'
import avatar1 from 'public/images/portrait/small/avatar-s-5.jpg'
import avatar2 from 'public/images/portrait/small/avatar-s-6.jpg'
import avatar3 from 'public/images/portrait/small/avatar-s-7.jpg'
import { MoreVertical, Edit, Edit2, Trash, CheckCircle, XCircle, EyeOff, CornerUpLeft, RefreshCw, Clipboard } from 'react-feather'
import { Table, Badge, UncontrolledDropdown, DropdownMenu, DropdownItem, DropdownToggle } from 'reactstrap'

const avatarGroupData1 = [
  {
    title: 'Lilian',
    img: avatar1,
    imgHeight: 26,
    imgWidth: 26
  },
  {
    title: 'Alberto',
    img: avatar2,
    imgHeight: 26,
    imgWidth: 26
  },
  {
    title: 'Bruce',
    img: avatar3,
    imgHeight: 26,
    imgWidth: 26
  }
]

const avatarGroupData2 = [
  {
    title: 'Diana',
    img: avatar1,
    imgHeight: 26,
    imgWidth: 26
  },
  {
    title: 'Rey',
    img: avatar2,
    imgHeight: 26,
    imgWidth: 26
  },
  {
    title: 'James',
    img: avatar3,
    imgHeight: 26,
    imgWidth: 26
  }
]

const avatarGroupData3 = [
  {
    title: 'Lee',
    img: avatar1,
    imgHeight: 26,
    imgWidth: 26
  },
  {
    title: 'Mario',
    img: avatar2,
    imgHeight: 26,
    imgWidth: 26
  },
  {
    title: 'Oswald',
    img: avatar3,
    imgHeight: 26,
    imgWidth: 26
  }
]

const avatarGroupData4 = [
  {
    title: 'Christie',
    img: avatar1,
    imgHeight: 26,
    imgWidth: 26
  },
  {
    title: 'Barnes',
    img: avatar2,
    imgHeight: 26,
    imgWidth: 26
  },
  {
    title: 'Arthur',
    img: avatar3,
    imgHeight: 26,
    imgWidth: 26
  }
]

const TableBasic = () => {
  return (
    <Table responsive>
      <thead>
        <tr>
          <th>NO</th>
          <th>YEAR</th>
          <th>SBU</th>
          <th>PROJECT THEME</th>
          <th>PROJECT NAME</th>
          <th>TEAM NAME</th>
          <th>STATUS</th>
          <th>SCORING</th>
          <th>ACTIONS</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>
            <t className='align-middle font-weight-bold' style={{ color: "#ff9f43" }}>1</t>
          </td>
          <td>
            <t className='align-middle font-weight-bold' style={{ color: "#ff9f43" }}>2021</t>
          </td>
          <td>
            <t>Pharma</t>
          </td>
          <td>
            <t>Revenue Growth</t>
          </td>
          <td>
            <t>ACCELERATED STABILITY ASSESMENT PROGRAM</t>
          </td>
          <td>
            <t>ASAP</t>
          </td>
          <td>
            <Badge pill color='light-success' className='mr-1'>
              Submitted
            </Badge>
          </td>
          <td>
            <Clipboard></Clipboard>
          </td>
          <td>
            <UncontrolledDropdown>
              <DropdownToggle className='icon-btn hide-arrow' color='transparent' size='sm' caret>
                <MoreVertical size={15} />
              </DropdownToggle>
              <DropdownMenu right>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <Edit2 className='mr-50' size={15} /> <span className='align-middle'>View Details</span>
                </DropdownItem>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <CheckCircle className='mr-50' size={15} /> <span className='align-middle'>Submit</span>
                </DropdownItem>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <XCircle className='mr-50' size={15} /> <span className='align-middle'>Reject</span>
                </DropdownItem>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <EyeOff className='mr-50' size={15} /> <span className='align-middle'>Hide</span>
                </DropdownItem>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <RefreshCw className='mr-50' size={15} /> <span className='align-middle'>Revised</span>
                </DropdownItem>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <CornerUpLeft className='mr-50' size={15} /> <span className='align-middle'>Return</span>
                </DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
          </td>
        </tr>
        <tr>
        <td>
            <t className='align-middle font-weight-bold' style={{ color: "#ff9f43" }}>1</t>
          </td>
          <td>
            <t className='align-middle font-weight-bold' style={{ color: "#ff9f43" }}>2021</t>
          </td>
          <td>
            <t>Pharma</t>
          </td>
          <td>
            <t>Revenue Growth</t>
          </td>
          <td>
            <t>ACCELERATED STABILITY ASSESMENT PROGRAM</t>
          </td>
          <td>
            <t>ASAP</t>
          </td>
          <td>
            <Badge pill color='light-success' className='mr-1'>
              Submitted
            </Badge>
          </td>
          <td>
            <Clipboard></Clipboard>
          </td>
          <td>
            <UncontrolledDropdown>
              <DropdownToggle className='icon-btn hide-arrow' color='transparent' size='sm' caret>
                <MoreVertical size={15} />
              </DropdownToggle>
              <DropdownMenu right>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <Edit2 className='mr-50' size={15} /> <span className='align-middle'>View Details</span>
                </DropdownItem>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <CheckCircle className='mr-50' size={15} /> <span className='align-middle'>Submit</span>
                </DropdownItem>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <XCircle className='mr-50' size={15} /> <span className='align-middle'>Reject</span>
                </DropdownItem>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <EyeOff className='mr-50' size={15} /> <span className='align-middle'>Hide</span>
                </DropdownItem>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <RefreshCw className='mr-50' size={15} /> <span className='align-middle'>Revised</span>
                </DropdownItem>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <CornerUpLeft className='mr-50' size={15} /> <span className='align-middle'>Return</span>
                </DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
          </td>
        </tr>
        <tr>
        <td>
            <t className='align-middle font-weight-bold' style={{ color: "#ff9f43" }}>1</t>
          </td>
          <td>
            <t className='align-middle font-weight-bold' style={{ color: "#ff9f43" }}>2021</t>
          </td>
          <td>
            <t>Pharma</t>
          </td>
          <td>
            <t>Revenue Growth</t>
          </td>
          <td>
            <t>ACCELERATED STABILITY ASSESMENT PROGRAM</t>
          </td>
          <td>
            <t>ASAP</t>
          </td>
          <td>
            <Badge pill color='light-success' className='mr-1'>
              Submitted
            </Badge>
          </td>
          <td>
            <Clipboard></Clipboard>
          </td>
          <td>
            <UncontrolledDropdown>
              <DropdownToggle className='icon-btn hide-arrow' color='transparent' size='sm' caret>
                <MoreVertical size={15} />
              </DropdownToggle>
              <DropdownMenu right>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <Edit2 className='mr-50' size={15} /> <span className='align-middle'>View Details</span>
                </DropdownItem>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <CheckCircle className='mr-50' size={15} /> <span className='align-middle'>Submit</span>
                </DropdownItem>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <XCircle className='mr-50' size={15} /> <span className='align-middle'>Reject</span>
                </DropdownItem>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <EyeOff className='mr-50' size={15} /> <span className='align-middle'>Hide</span>
                </DropdownItem>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <RefreshCw className='mr-50' size={15} /> <span className='align-middle'>Revised</span>
                </DropdownItem>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <CornerUpLeft className='mr-50' size={15} /> <span className='align-middle'>Return</span>
                </DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
          </td>
        </tr>
        <tr>
        <td>
            <t className='align-middle font-weight-bold' style={{ color: "#ff9f43" }}>1</t>
          </td>
          <td>
            <t className='align-middle font-weight-bold' style={{ color: "#ff9f43" }}>2021</t>
          </td>
          <td>
            <t>Pharma</t>
          </td>
          <td>
            <t>Revenue Growth</t>
          </td>
          <td>
            <t>ACCELERATED STABILITY ASSESMENT PROGRAM</t>
          </td>
          <td>
            <t>ASAP</t>
          </td>
          <td>
            <Badge pill color='light-danger' className='mr-1'>
              Rejected
            </Badge>
          </td>
          <td>
            <Clipboard></Clipboard>
          </td>
          <td>
            <UncontrolledDropdown>
              <DropdownToggle className='icon-btn hide-arrow' color='transparent' size='sm' caret>
                <MoreVertical size={15} />
              </DropdownToggle>
              <DropdownMenu right>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <Edit2 className='mr-50' size={15} /> <span className='align-middle'>View Details</span>
                </DropdownItem>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <CheckCircle className='mr-50' size={15} /> <span className='align-middle'>Submit</span>
                </DropdownItem>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <XCircle className='mr-50' size={15} /> <span className='align-middle'>Reject</span>
                </DropdownItem>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <EyeOff className='mr-50' size={15} /> <span className='align-middle'>Hide</span>
                </DropdownItem>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <RefreshCw className='mr-50' size={15} /> <span className='align-middle'>Revised</span>
                </DropdownItem>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <CornerUpLeft className='mr-50' size={15} /> <span className='align-middle'>Return</span>
                </DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
          </td>
        </tr>
        <tr>
        <td>
            <t className='align-middle font-weight-bold' style={{ color: "#ff9f43" }}>1</t>
          </td>
          <td>
            <t className='align-middle font-weight-bold' style={{ color: "#ff9f43" }}>2021</t>
          </td>
          <td>
            <t>Pharma</t>
          </td>
          <td>
            <t>Revenue Growth</t>
          </td>
          <td>
            <t>ACCELERATED STABILITY ASSESMENT PROGRAM</t>
          </td>
          <td>
            <t>ASAP</t>
          </td>
          <td>
            <Badge pill color='light-secondary' className='mr-1'>
              Hidden
            </Badge>
          </td>
          <td>
            <Clipboard></Clipboard>
          </td>
          <td>
            <UncontrolledDropdown>
              <DropdownToggle className='icon-btn hide-arrow' color='transparent' size='sm' caret>
                <MoreVertical size={15} />
              </DropdownToggle>
              <DropdownMenu right>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <Edit2 className='mr-50' size={15} /> <span className='align-middle'>View Details</span>
                </DropdownItem>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <CheckCircle className='mr-50' size={15} /> <span className='align-middle'>Submit</span>
                </DropdownItem>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <XCircle className='mr-50' size={15} /> <span className='align-middle'>Reject</span>
                </DropdownItem>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <EyeOff className='mr-50' size={15} /> <span className='align-middle'>Hide</span>
                </DropdownItem>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <RefreshCw className='mr-50' size={15} /> <span className='align-middle'>Revised</span>
                </DropdownItem>
                <DropdownItem href='/' onClick={e => e.preventDefault()}>
                  <CornerUpLeft className='mr-50' size={15} /> <span className='align-middle'>Return</span>
                </DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
          </td>
        </tr>
      </tbody>
    </Table>
  )
}

export default TableBasic
