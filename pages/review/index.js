import { Fragment } from 'react'
import { Row, Col, Label, CustomInput, Input, Card } from 'reactstrap'
import Table from './Table'
import ReactPaginate from 'react-paginate'

const Review = () => {
  return(
    <Fragment>
        <Row>
          <Col sm='12'>
            <Card style={{ padding: '20px', borderRadius: '10px' }}>
              <t style={{ color: '#5e5873', fontSize: '24px', fontWeight: 'bold' }}>Filter</t>
              <Col xl='6' className='d-flex align-items-center p-0'>
                <div className='d-flex align-items-center w-100'>
                  <CustomInput
                    className='form-control mx-50'
                    type='select'
                    id='rows-per-page'
                    value={""}
                    onChange={""}
                    style={{
                      width: '25rem',
                      padding: '0 0.8rem',
                      backgroundPosition: 'calc(100% - 3px) 11px, calc(100% - 20px) 13px, 100% 0',
                    }}
                  >
                      <option value='Year' style={{ color: '#b9b9c3' }}>Year</option>
                    </CustomInput>
                </div>
              </Col>
              <Col xl='6' className='d-flex align-items-center p-0'>
                <div className='d-flex align-items-center w-100'>
                  <CustomInput
                    className='form-control mx-50'
                    type='select'
                    id='rows-per-page'
                    value={""}
                    onChange={""}
                    style={{
                      width: '25rem',
                      padding: '0 0.8rem',
                      backgroundPosition: 'calc(100% - 3px) 11px, calc(100% - 20px) 13px, 100% 0',
                    }}
                  >
                      <option value='SubGroupCategory' style={{ color: '#b9b9c3' }}>Sub Group Category</option>
                    </CustomInput>
                </div>
              </Col>
            </Card>

            <Card style={{ padding: '20px', borderRadius: '10px' }}>
              <Col xl='6' className='d-flex align-items-center p-0'>
                <div className='d-flex align-items-center w-100'>
                  <Label for='rows-per-page'>Show</Label>
                  <CustomInput
                    className='form-control mx-50'
                    type='select'
                    id='rows-per-page'
                    value={""}
                    onChange={""}
                    style={{
                      width: '5rem',
                      padding: '0 0.8rem',
                      backgroundPosition: 'calc(100% - 3px) 11px, calc(100% - 20px) 13px, 100% 0',
                      borderStyle: 'none'
                    }}
                  >
                      <option value='10'>10</option>
                      <option value='25'>25</option>
                      <option value='50'>50</option>
                    </CustomInput>
                </div>
              </Col>
              <Col
                xl='6'
                className='d-flex align-items-sm-center justify-content-lg-end justify-content-start flex-lg-nowrap flex-wrap flex-sm-row flex-column pr-lg-1 p-0 mt-lg-0 mt-1'
              >
                <div className='d-flex align-items-center mb-sm-0 mb-1 mr-1'>
                  <Label className='mb-0' for='search-invoice'>
                    Search:
                  </Label>
                  <Input
                    id='search-invoice'
                    className='ml-50 w-100'
                    type='text'
                    value={""}
                    onChange={""}
                  />
                </div>
              </Col>
            </Card>

            <Card style={{ padding: '20px', borderRadius: '10px' }}>
              <Table />
              <Row>
                <Col>
                  <t style={{ color: "#b9b9c3" }}>Showing 1 to 10 of 29 entries</t>
                </Col>
                <Col md={{ span: 3, offset: 10 }}>
                  <ReactPaginate
                    pageCount={7}
                    nextLabel={''}
                    breakLabel={'...'}
                    pageRangeDisplayed={5}
                    marginPagesDisplayed={2}
                    activeClassName={'active'}
                    pageClassName={'page-item'}
                    previousLabel={''}
                    nextLinkClassName={'page-link'}
                    nextClassName={'page-item next-item'}
                    previousClassName={'page-item prev-item'}
                    previousLinkClassName={'page-link'}
                    pageLinkClassName={'page-link'}
                    breakClassName='page-item'
                    breakLinkClassName='page-link'
                    containerClassName={'pagination react-paginate'}
                  />
                </Col>
              </Row>
            </Card>
          </Col>
        </Row>
    </Fragment>
  )
}

export default Review
