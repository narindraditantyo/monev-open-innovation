import { useState } from "react";
import {
  Card,
  CardBody,
  Container,
  Nav,
  NavItem,
  NavLink,
  TabContent,
  TabPane,
} from "reactstrap";
import Assessment from "pages/assessment";
import ProjectSum from "pages/project_summary";

const tableColumns = [
  {
    name: "Item",
    selector: "item",
    sortable: false,
    minWidth: "250px",
  },
  {
    name: "Assessment Criteria",
    selector: "assesment_criteria",
    sortable: false,
    minWidth: "250px",
  },
  {
    name: "Score",
    selector: "score",
    sortable: false,
    minWidth: "150px",
  },

  {
    name: "Weight",
    selector: "weight",
    sortable: false,
    minWidth: "150px",
  },
  {
    name: "Total",
    selector: "total",
    sortable: false,
    minWidth: "100px",
  },
];

const ReviewDetails = () => {
  const [active, setActive] = useState("assessment");

  const toggleTab = (tab) => {
    active !== tab && setActive(tab);
  };

  return (
    <Container className="my-2">
      <Card>
        <Nav tabs>
          <NavItem>
            <NavLink
              active={active === "project_summary"}
              className="py-1 h5"
              onClick={() => toggleTab("project_summary")}
            >
              Project Summary
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              active={active === "assessment"}
              className="py-1 h5"
              onClick={() => toggleTab("assessment")}
            >
              PIC BU Assessment
            </NavLink>
          </NavItem>
        </Nav>
        <CardBody>
          <TabContent activeTab={active}>
            <TabPane tabId="project_summary">
              <ProjectSum />
            </TabPane>
            <TabPane tabId="assessment">
              <Assessment />
            </TabPane>
          </TabContent>
        </CardBody>
      </Card>
    </Container>
  );
};

export default ReviewDetails;
